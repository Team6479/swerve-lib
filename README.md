[![](https://jitpack.io/v/Team6479/swerve-lib-2022-unmaintained.svg)](https://jitpack.io/#Team6479/swerve-lib-2022-unmaintained)

### Installation

Add the following to your `build.gradle`:

```
repositories {
    maven {
        url 'https://jitpack.io'
    }
}

dependencies {
    implementation 'com.github.Team6479:swerve-lib-2022-unmaintained:develop-SNAPSHOT'
}

```

Build the code
